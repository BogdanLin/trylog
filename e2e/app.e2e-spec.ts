import { AuthentificationPage } from './app.po';

describe('authentification App', () => {
  let page: AuthentificationPage;

  beforeEach(() => {
    page = new AuthentificationPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
